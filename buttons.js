//A script page that handles button clicks
//Jakob Coles
//May 21, 2018
var pressed=0; //js is weakly typed and uses type inference
var i;
var board = [0,0,0,0,0,0,0,0,0];
var win = false;
function cler(){ //clears board
    for(i = 0; i < 9; i++){
    	board[i] = 0;
        //Accesses an HTML element by its ID and changes a property
        //(in this case inner HTML)
        document.getElementById("button"+i).innerHTML = "_";
    }
    //but we can change *any* property of an HTML element with document.getElementById()
    document.getElementById("thepic").src = "";
    win = false;
    pressed = 0;
}
function won2(boo){ //does stuff if someone wins
    var winstring = (boo==1?"X":"O")+" has won!";
    win = true;
    window.alert(winstring);
    document.getElementById("thepic").src = "https://i.imgur.com/RbCKrf8.gif";
}
function won(boo){ //checks for a winner
    //much of the basic js syntax is the same as Java
    if(board[0] == boo && board[1] == boo && board[2] == boo) won2(boo);
    if(board[3] == boo && board[4] == boo && board[5] == boo) won2(boo);
    if(board[6] == boo && board[7] == boo && board[8] == boo) won2(boo);
    if(board[0] == boo && board[3] == boo && board[6] == boo) won2(boo);
    if(board[1] == boo && board[4] == boo && board[7] == boo) won2(boo);
    if(board[2] == boo && board[5] == boo && board[8] == boo) won2(boo);
    if(board[0] == boo && board[4] == boo && board[8] == boo) won2(boo);
    if(board[2] == boo && board[4] == boo && board[6] == boo) won2(boo);
    if(pressed == 9){
        win = true;
        window.alert("Tie game!"); //makes browser display a message box
    }
}
function press(idx){ //event handler for the game board's buttons
    //If someone tries to break the rules, we must insult them appropriately.
    if(win){
        window.alert("The game is over, you bawdy bat-fowling flap-dragon!");
        return;
    }
    var my_button = "button"+idx;

    if(document.getElementById(my_button).innerHTML != "_"){
        window.alert("Take an unoccupied square, you mangled dismal-dreaming malt-worn!");
    }
    //places an X or an O as appropriate in the square that was pressed
    else{
        if(pressed%2===0){
            document.getElementById(my_button).innerHTML = "X";
            board[idx]=1;
        }
        else{
            document.getElementById(my_button).innerHTML = "O";
            board[idx]=2;
        }    
        pressed++;
    }
    if(pressed > 4) won(1+(pressed-1)%2);
}